$("#update").hide();

assignDataToTable();

function deleteStudent(e) {
    var id = $(e).closest('tr').children('td:first').text();
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8080/v1/student/" + id,
        success: function (data) {
            alertUsing("ลบแล้ว", true);
            assignDataToTable();
        },
        error: function (err) {
            console.log(err);
            alert(err);
        }
    });
}

function editStudent(e) {
    var id = $(e).closest('tr').children('td:first').text();
    var name = $(e).closest('tr').children('td:nth-child(2)').text();
    var age = $(e).closest('tr').children('td:nth-child(3)').text();

    $("#id").val(id);
    $("#name").val(name);
    $("#age").val(age);

    $("#update").show();
    $("#save").hide();
}

function updateStudent() {
    var id = $("#id").val();
    var ageNum = parseInt($("#age").val());

    var jsonVar = {
        name: $("#name").val(),
        age: ageNum
    };

    $.ajax({
        type: "PUT",
        data: JSON.stringify(jsonVar),
        contentType: "application/json",
        url: "http://localhost:8080/v1/student/" + id,
        success: function (data) {
            alertUsing("แก้ไขแล้ว", true);
            $("#update").hide();
            $("#save").show();
            $("#id").val("");
            $("#name").val("");
            $("#age").val("");
            assignDataToTable();
        },
        error: function (err) {
            console.log(err);
            alert(err);
        }
    });
}

function saveStudent() {

    var jsonVar = {
        name: $("#name").val(),
        age: $("#age").val()
    };

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/v1/student",
        data: JSON.stringify(jsonVar),
        contentType: "application/json",
        success: function (data) {
            assignDataToTable();
            $("#name").val("");
            $("#age").val("");
            alertUsing("บันทึกแล้ว", true)
        },
        error: function (err) {
            console.log(err);
            alert(err);
        }
    });

}

function assignDataToTable() {
    $("tbody").empty();
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "http://localhost:8080/v1/student",
        success: function (data) {
            var students = JSON.parse(JSON.stringify(data));
            for (var i in students) {
                $("tbody").
                append("<tr> \
                            <td>" + students[i].id + "</td> \
                            <td>" + students[i].name + "</td> \
                            <td>" + students[i].age + "</td> \
                            <td> \ <button onclick='deleteStudent(this)' class=' btn btn-danger'>Delete</button> \
                           <button onclick='editStudent(this)' class=' btn btn-warning'>Edit</button> \ </td> \
                        </tr>");
            }
        },
        error: function (data) {
            console.log(data);
        }
    });

}

function alertUsing(text, flag) {

    var alert = $(".alert");

    if (flag) {
        alert.removeClass("alert-danger").addClass("alert-success");
    } else {
        alert.removeClass("alert-success").addClass("alert-danger");

    }

    alert.fadeIn(400);
    alert.css("display", "block");
    alert.text(text);
    setTimeout(function () {
        alert.fadeOut();
    }, 2000);

}